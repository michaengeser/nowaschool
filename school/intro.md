# Welcome

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/julia-pfarr%2Fnowaschool_material/main)
[![License](https://img.shields.io/gitlab/license/julia-pfarr/nowaschool_material)](https://gitlab.com/julia-pfarr/nowaschool_material)
[![Contributors](https://img.shields.io/gitlab/contributors/julia-pfarr%2Fnowaschool_material)](https://gitlab.com/julia-pfarr/nowaschool_material)
[![Issues](https://img.shields.io/gitlab/issues/all/julia-pfarr%2Fnowaschool_material)](https://gitlab.com/julia-pfarr/nowaschool_material/-/issues)
[![PRs](https://img.shields.io/gitlab/merge-requests/all/julia-pfarr%2Fnowaschool_material)](https://gitlab.com/julia-pfarr/nowaschool_material/-/merge_requests)

Hello everyone and welcome to the [NOWA](https://sfbs.pages.uni-marburg.de/sfb135/nowa/nowa.site/) winter school!

Within these pages, we provide information on how to follow the workshop, as well as respective materials. This [jupyter book](https://jupyterbook.org/intro.html) will include the used slides and code in a way that they can be explored in an interactive manner. You can navigate through the respective sections via the TOC on the left side and within sections via the TOC on the right side. The three symbols in the top allow to enable full screen mode, link to the underlying [GitLab repository](https://gitlab.com/julia-pfarr/nowaschool_material) and allow you to download the contents as a pdf or jupyter notebook respectively. All of this awesomeness (talking about the infrastructure and resource) is possible through the dedicated and second to none work of the [Jupyter community](https://jupyter.org/community), specifically, the [Executable/Jupyter Book](https://executablebooks.org/en/latest/) and [mybinder project](https://mybinder.org/).

````{margin}
```{warning}
These pages are currently under construction and will be updated continuously.
Please visit this page again in the next few weeks for further information.
````

## The NOWA school
  
You can checkout the respective sections:

* [An overview](overview.md)

   What's this winter school about and how is it organized?

* [Setup](setup.md)

   How are things implemented and supposed to work?

* [General outline](overview.md)

   What are the specific topics and aspects taught?

* [Prerequisites](prerequisites.md)

   All things gotta start somewhere and the content of this winter school is of course no exceptions to that, especially since a certain amount of digital literacy, programming, etc. is required. 
   Here, we gathered some resources participants can check out in preparation for the course or just for fun.

* [Research Data management](materials/RDM/overview_RDM.md)

* [Running experiments via Psychopy](materials/psychopy/overview_psychopy.md)

* [Version control via Git and GitLab](materials/Git/overview_Git.md)

* [Introduction to Python](materials/python/overview_python.md)

* [Continuous integration, development and clean code](materials/CI_CD/overview_CI_CD.md)

* [Code of Conduct](CoC.md)

   Necessities for creating an open, fair, safe and inclusive learning
   experience.

# I've got a question!

In case you have any questions or difficulties going through the winter school, please don’t hesitate a single second to get in touch with
us. A great way to do this is to
[open an issue](https://gitlab.com/julia-pfarr/nowaschool_material/-/issues) on the
[GitLab site of the Workshop](https://gitlab.com/julia-pfarr/nowaschool_material) (also possible via the GitHub button at the top of the pages).
We would also highly appreciate and value every feedback or idea or you
might have (via [issues](https://gitlab.com/julia-pfarr/nowaschool_material/-/issues) or [hypothes.is annotation feature](https://web.hypothes.is/) on the right).

## Acknowledgements

This winter school was organized by Peer Herholz and Julia-Katharina Pfarr.

Peer Herholz' work on  this workshop was enabled through training received at the [Montreal Neurological Institute](https://www.mcgill.ca/neuro/), specifically the [NeuroDataScience - ORIGAMI lab](https://neurodatascience.github.io/) supported by funding from the Canada First Research Excellence Fund, awarded to McGill University for the [Healthy Brains for Healthy Lives initiative](https://www.mcgill.ca/hbhl/), the [National Institutes of Health (NIH)](https://www.nih.gov/) [NIH-NIBIB P41 EB019936 (ReproNim)](https://www.repronim.org/), the [National Institute Of Mental Health](https://www.nimh.nih.gov/) of the NIH under Award  Number [R01MH096906 (Neurosynth)](https://www.neurosynth.org/), a [research scholar award from Brain Canada, in partnership with Health Canada, for the Canadian Open Neuroscience Platform initiative](https://conp.ca/), as well as an [Excellence Scholarship from Unifying Neuroscience and Artificial Intelligence - Québec](https://sites.google.com/view/unique-neuro-ai).

Julia-Katharina Pfarr's work on this school was enabled through her work as a Data Steward in the [SFB 135](https://www.sfb-perception.de/index-en/) "Cardinal Mechanisms of Perception", infrastructure project [NOWA](https://sfbs.pages.uni-marburg.de/sfb135/nowa/nowa.site/). Julia is also part of the "TAM Data Hub" team and supported by ["The Adaptive Mind"](https://www.theadaptivemind.de/) consortium. 


|||
|---|---|
|![SFB 135](materials/static/sfb.png)|![TAM](materials/static/tam_logo.png)|