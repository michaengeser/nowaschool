# Python

## Topics 💡

In the following you'll find the `objectives` and `materials` for each of the topics we'll discuss during this session.

### The Jupyter Ecosystem

We had a look at [different ways of communicating with our computers](https://julia-pfarr.gitlab.io/nowaschool/prerequisites.html#guis-vs-clis). Generally speaking we could either use a `GUI` or a `CLI`. In more detail we also have different options to utilize the same `programming language` to communicate with our `computer`, e.g. the `shell`, `jupyter notebooks` and `IDE`s. After we already explored the `shell` a bit, we'll now spend a closer look at `jupyter notebooks` and how we can work with them. This is a central topic as the  `data analyzes` parts of the course will be conducted via `jupyter notebooks` and you'll an increasing number of `tutorials`, `workshops` and `publications` done via `Jupyter notebooks` and the wider `Jupyter ecosystem`.  


`````{admonition} Objectives📍
:class: tip
- learn basic and efficient usage of the `jupyter ecosystem` & `notebooks`
    - what is `Jupyter` & how to utilize `jupyter notebooks`
- Ask and answer questions
- Have a great time
`````

#### Materials📓

Please have a look at the section [The Jupyter Ecosystem & notebooks](https://julia-pfarr.gitlab.io/nowaschool/materials/python/intro_jupyter.html) in the `ToC` on the left.

### Introduction to Data analyzes - I

Sure, most of us already conducted some analyses here and there but did you already make use of the fantastic world of `data analyzes` using `python` with its tremendously powerful go-to `modules` and basically unlimited amount of highly specifialized libraries? No matter what you're looking for and want to work on, `python`'s got your back! Given the short amount of time we have, this session will focus on one of the central `modules` concerning working with data: [pandas](https://pandas.pydata.org/). In more detail, we will check the `data` we obtained during the `PsychoPy` sessions via going through several core steps of `data analyes` using `pandas`.   

`````{admonition} Objectives📍
:class: tip
- learn basic and efficient usage of `python` for `data analyzes` & `visualization`
    - working with `data`: 
        - `reading`, `working`, `writing`
        - `preprocessing`, `filtering`, `wrangling`
    - `visualizing` `data`:
        - basic plots
        - advanced & fancy stuff
    - `statistical analyses`    
- Ask and answer questions
- Have a great time
`````

#### Materials📓

Please have a look at the section [Introduction to Data analyzes - I](https://julia-pfarr.gitlab.io/nowaschool/materials/python/intro_data_analyzes_visualization.html) in the `ToC` on the left.


### Introduction to Data analyses - II
Sure, most of us already conducted some analyses here and there but did you already make use of the fantastic world of `data analyzes` using `python` with its tremendously powerful go-to `modules` and basically unlimited amount of highly specialized `libraries`? No matter what you're looking for and want to work on, `python`'s got your back! Given the short amount of time we have, this session will focus on one of the central `modules` concerning `data visualization` and `statistical analyses`. In more detail, we will `visualize` and `analyse` the `data` we obtained during the `PsychoPy` sessions via different means.        

#### Objectives 📍

`````{admonition} Objectives📍
:class: tip
- learn basic and efficient usage of `python` for `data analyzes` & `visualization`
    - working with `data`: 
        - `reading`, `working`, `writing`
        - `preprocessing`, `filtering`, `wrangling`
    - `visualizing` `data`:
        - basic plots
        - advanced & fancy stuff
    - `statistical analyses`    
- Ask and answer questions
- Have a great time
`````

#### Materials 📓

Please have a look at the section [Introduction to Data analyzes - I](https://julia-pfarr.gitlab.io/nowaschool/materials/python/intro_data_analyzes_visualization_II.html) in the `ToC` on the left.


### Python module showcases

The reality of courses like this one is that they really can't teach attending folks all they need to know to subsequently apply the adjacent content sufficiently, not to mention the training aspect. At best, courses like this one provide folks with the materials, overview and pointers necessary to continue and actually dive into the taught topics (the truth hurts sometimes, doesn't it?). Another important aspect that is frequently overlooked is that attendees usually need a certain set of skills and experience to really benefit from a given course, especially considering the diverse backgrounds folks have. We want to be fair, open and welcoming to everyone and while we know that we unfortunately can't achieve and assume a truly similar level of training and experience across all participants, we can at least do our best to work towards certain opportunities. That being said, courses like the one you're currently looking at, quite often (should) provide add-on materials covering and teaching basic to advanced programming skills applied to `file handling`/`input`/`output`, `signal processing`, `statistics`, `linear algebra` (`vectors` much?) and certain `data modalities`. 

As we unfortunately can't talk about all the cool things out there wrt `analyzes`, etc. and everyone works on different projects with different `data` and `aims`, we decided to leave the last part of this session open and provide a couple of showcases, providing an overview/introduction to a broad collection of `python modules`, including "general" and "specialized" applications.

Thus, please have a look at the different `materials` provided [here](https://julia-pfarr.gitlab.io/nowaschool/materials/python/showcases) and check out what interests you. Please let us know if you have any questions.

### optional/reading/further materials

#### Python courses

Here's a list of fantastic other `python` introduction courses with a focus on working with `data`:

https://statsthinking21.github.io/statsthinking21-python/

https://valdanchev.github.io/reproducible-data-science-python/intro.html 

https://psychology.nottingham.ac.uk/staff/lpzjd/psgy1001-21/ 

https://shawnrhoads.github.io/gu-psyc-347/ 

