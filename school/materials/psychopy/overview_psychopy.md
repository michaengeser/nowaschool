# Psychopy

Within this part of the winter school, we will explore how to create and run experiments using [PsychoPy](https://www.psychopy.org/), a `python library` dedicated to conducting experiments in the realm of `psychology` and adjacent fields. Due to time constraints we will only spend a quick look at things and showcase the most important aspects to allow folks to explore things further after the course ends.


## Topics 💡

In the following you'll find the `objectives` and `materials` for each of the topics we'll discuss during this session.

### Introduction to PsychoPy - Setup  & Basics 
Most of us already conducted one or the other experiment. However, there's a fair number of different software options and tools out there, which mostly depends on the computational infrastructure at hand but also on what people are used to. That being said: what options are roughly out there and can we actually use `python` based resources concerning experiments with a clear conscience? To get some idea we will explore the `python library` [PsychoPy](), including its setup, basic working principles, as well as advantages and disadvantages, showcased on a very simple experiment. Buckle up y'all, this is going to be quite a ride.      

`````{admonition} Objectives📍
:class: tip
- running `experiments` using `python`
    - Exploring `PsychoPy`
    - basic setup & interaction
    - `experiments`, `trials`, `stimuli`, `responses`
    - a very simple experiment
- Ask and answer questions
- Have a great time
`````

#### Materials📓

Please have a look at the section [Introduction to PsychoPy](https://julia-pfarr.gitlab.io/nowaschool/materials/psychopy/introduction_psychopy.html#id1) in the `ToC` on the left.


### Introduction to PsychoPy - Presenting stimuli & trials



#### Materials📓

Please have a look at the section [PsychoPy - stimuli & trials](https://julia-pfarr.gitlab.io/nowaschool/materials/psychopy/introduction_psychopy.html#psychopy) in the `ToC` on the left.



### Introduction to PsychoPy - Collecting responses


#### Materials📓

Please have a look at the section [PsychoPy - data output](https://julia-pfarr.gitlab.io/nowaschool/materials/psychopy/introduction_psychopy.html#data-output) in the `ToC` on the left.


### Introduction to PsychoPy - A simple experiment


#### Materials📓

Please have a look at the section [PsychoPy - A simple experiment](https://julia-pfarr.gitlab.io/nowaschool/materials/psychopy/introduction_psychopy.html#a-very-simple-experiment) in the `ToC` on the left.


### Introduction to PsychoPy - Advanced topics


#### Materials📓

Please have a look at the section [PsychoPy - advanced](https://julia-pfarr.gitlab.io/nowaschool/materials/psychopy/introduction_psychopy.html#psychopy-advanced) in the `ToC` on the left.

### optional/reading/further materials

#### PsychoPy resources

The [PsychoPy website](https://www.psychopy.org/) with all the information and [tutorials/workshops](https://workshops.psychopy.org/).

<iframe width="560" height="315" src="https://www.psychopy.org/" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

The [PsychoPy discourse forum](https://discourse.psychopy.org/) many very helpful Q&As and for direct support.

<iframe width="560" height="315" src="https://discourse.psychopy.org/" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

#### Things to check out

Here are some cool videos providing some further insights into the world and capabilities of `PsychoPy`.

First, a nice tutorial on how to create a “`line bisection task`” using `PsychoPy`.

<iframe width="560" height="315" src="https://www.youtube.com/embed/dQSL_ogaLG8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Want to know how far you can go with `PsychoPy`? How about coding an entire video game? Yeah, that's possible!

<iframe width="560" height="315" src="https://www.youtube.com/embed/ISHAnDF4Xkc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>