# Git and GitLab

## Topics 💡

In the following you'll find the `objectives` and `materials` for each of the topics we'll discuss during this session.

### Version Control and Git: Background and Theory

`````{admonition} Objectives📍
:class: tip
- What Version Control is and where it comes from
- Why it is useful
- The basic principles of Git:
    - the Git database
    - structure of the Git repository
    - the staging area
    - commits
`````

#### Materials📓

- Sub-section [Version Control and Git](sections/intro.md)

### Basic Git Workflow

`````{admonition} Objectives📍
:class: tip
- init
- add
- commit
- status
- diff
- log
`````

#### Materials📓

-Sub-section [Basic workflow](sections/basics.md)

### Branching and merging

`````{admonition} Objectives📍
:class: tip
- branch
- checkout
- merge
- HEAD and detached HEAD
- moving in your commit history
- how to solve a merge conflict
`````

#### Materials📓

- Sub-section [Branching and Merging](sections/branching_merging.md)

### GitLab workflow

`````{admonition} Objectives📍
:class: tip
- What is GitLab
- local vs. remote repository
- push
- pull
- clone
- fetch
- merge
- merge conflict between your local and remote repository
`````

#### Materials📓

- Sub-section [GitLab](sections/gitlab.md)

### Contributing on GitLab

`````{admonition} Objectives📍
:class: tip
- fork
- merge requests
- gitlab for project management
- git LFS
`````

#### Materials📓

- Sub-section [Contributing](sections/contribute.md)


### optional/reading/further materials

- YouTube is full of Git/GitLab/GitHub videos for all kinds of levels and features!!! For example: Brainhack [Git introduction](https://youtu.be/fBgxmpk9C4I?si=im33MV2V0PV8uM0k) or [GitHub CI](https://youtu.be/VibDC49ZAJE?si=587WHWe1nZQzHIay)
- Git cheat sheet by [gitlab](https://about.gitlab.com/images/press/git-cheat-sheet.pdf) or [github](https://education.github.com/git-cheat-sheet-education.pdf)
- [Atlassian tutorials](https://www.atlassian.com/git/tutorials) and [cheat sheet](https://www.atlassian.com/git/tutorials/atlassian-git-cheatsheet)
- Troubleshooting: [Oh shit git](https://ohshitgit.com/) or [Dangit git](https://dangitgit.com/) (are the same, but the latter is without swearing)
- [Git branching](https://learngitbranching.js.org/?locale=de_DE)
- [Git GUIs](https://git-scm.com/downloads/guis) 
- [Advanced Git commands](https://www.atlassian.com/git/tutorials/advanced-overview)
- really, just type anything you want to know about Git in YouTube and you'll find a tutorial for it. I promise. 