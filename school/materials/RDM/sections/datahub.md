## The TAM Data Hub

The Data Hub is an infrastructure project of the "The Adaptive Mind" (TAM) consortium. Guided by the "FAIR" principles, the Data Hub offers resources, services and support to affiliated researches on various levels to ease collaborative work:

- Central storage and compute resources: MaSC / MaRC3.
- Services that allow efficient usage of these resources: GitLab, JupyterHub, data repository. 
- Support on how to use the services in a way that fits the needs of the individual project.

### Architecture and workflow

![datahub](../images/datahub.png)

**IMPORTANT NOTE**: Usually, platforms like gitlab.com or github.com are NOT made for large data storage. Those platforms are primarily for collaborative code development. If you use gitlab.com or github.com, your data needs to be stored somewhere else (at least when you work with big data such as MRI data. If your data is just one tabular sheet, you're fine.). In the TAM Data Hub we made it possible that our own GitLab platform can hold large data as it is connected to our own storage. We did this to enable a simple data management workflow for you.

### Access and Onboarding

Please see the [Data Hub User Manual](https://verbundprojekte.pages.uni-marburg.de/tam/datahub/UserManual/Support/datahub_access/) for additional information.

- Uni Marburg Staff account for Non-Uni-Marburg TAM members --> Account request
    - researchers who want to work with the Data Hub need a staff email account at Marburg University
    - if you are not primarily affiliated with Marburg University, an account needs to be created for you 
    - for this, please reach out to datahub@uni-marburg.de and provide information about yourself which is listed in the [Data Hub User Manual](https://verbundprojekte.pages.uni-marburg.de/tam/datahub/UserManual/Support/datahub_access/)
- 2-Factor Authentication after receiving your Uni-Marburg Staff Account
    - you will need to actively apply for a 2-factor authentication on the website 
    - after you received your 2-FA, you can use the Data Hub services with your email account, password, and 2-FA.

After you have the needed account, please reach out to datahub@uni-marburg.de again in order to make an appointment with one of the Data Stewards. We would like to have a first chat with you about your needs (e.g., which services do you want to use and a first introduction to the usage of these services).
