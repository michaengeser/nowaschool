# Research Data Management (RDM)

## Topics 💡

In the following you'll find the `objectives` and `materials` for each of the topics we'll discuss during this session.

### Introduction to RDM 

`````{admonition} Objectives📍
:class: tip
- What Research Data Management (RDM) means and why it is important
- The FAIR principles
- The Research Life Cycle
- RDM & Open Science
- Overview of RDM components
- Introduction of our example project
`````

#### Materials📓

- Sub-section [Introduction to RDM](sections/introduction.md)

### The Open Science Framework (OSF)

`````{admonition} Objectives📍
:class: tip
- What OSF is and why it is useful
- First settings for an OSF project
`````

#### Materials📓

- Sub-section [OSF](sections/osf.md)
- [OSF](https://osf.io/)

### Research Data Management Plan (RDMP)

`````{admonition} Objectives📍
:class: tip
- What a RDMP is
- Why it is important
- RDMP components
- Research Data Management Organiser (RDMO) 
`````

#### Materials📓

- Sub-section [RDMP](sections/rdmp.md)
- [RDMP Template](https://docs.google.com/document/d/1F8u1jZFQohW6hj930Vg1rzl4VSbAxJsH/edit?usp=drive_link&ouid=108187111723793053254&rtpof=true&sd=true)

### Pre-registration

`````{admonition} Objectives📍
:class: tip
- What a pre-registration is
- Why it is important
- How to do it on OSF
- Task: pre-registration of our example project
`````

#### Materials📓

- Sub-section [Pre-registration](sections/preregistration.md)
- [OSF Pre-registration](https://help.osf.io/article/145-preregistration)

### Project and Data Organization

`````{admonition} Objectives📍
:class: tip
- the TONIC Template
- BIDS
`````

#### Materials📓

- Sub-section [Project and Data Organization](sections/organization.md)
- [TONIC](https://gin-tonic.netlify.app/) website
- [BIDS](https://bids.neuroimaging.io/)
- [BIDS specification](https://bids-specification.readthedocs.io/en/stable/)
- [BIDS validator](https://bids-standard.github.io/bids-validator/)
- [BIDS Apps](https://bids-apps.neuroimaging.io/)

### Metadata

`````{admonition} Objectives📍
:class: tip
- general Metadata (bibliographic/administrative Metadata)
- content and field specific Metadata (BIDS)
`````

#### Materials📓

- Sub-section [Metadata](sections/metadata.md)
- [BIDS specification](https://bids-specification.readthedocs.io/en/stable/)

### Data Storage and Sharing

`````{admonition} Objectives📍
:class: tip
- different storage providers
- what to consider when sharing data
- Licenses
- Copyright 
`````

#### Materials📓

- Sub-section [Data Storage and Sharing](sections/storage_sharing.md)

### The TAM Data Hub

`````{admonition} Objectives📍
:class: tip
- overview of the TAM Data Hub resources and workflow
`````

#### Materials📓

- [Data Hub User Manual](https://verbundprojekte.pages.uni-marburg.de/tam/datahub/UserManual/DataHub/about_datahub/)

### optional/reading/further materials

Information and Training:

- [NOWA Blog](https://sfbs.pages.uni-marburg.de/sfb135/nowa/nowa.site/post/) and [tutorials](https://sfbs.pages.uni-marburg.de/sfb135/nowa/nowa.site/tutorial/)
- [The Turing Way Handbook](https://the-turing-way.netlify.app/reproducible-research/rdm)
- [UK Data Service](https://ukdataservice.ac.uk/learning-hub/research-data-management/)
- [HeFDI workshops, materials, and publications](https://www.uni-marburg.de/en/hefdi/about-hefdi)
- [forschungsdaten.info](https://forschungsdaten.info/themen/informieren-und-planen/)
- [OHBM Open Science Educationals](UChvSitFvqGDeA1y7MJs4CGQ)

Tools:

- [OSF](https://osf.io/)
- [RDMO University Marburg](https://rdmo.uni-marburg.de/)

